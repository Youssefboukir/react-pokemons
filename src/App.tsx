import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import { Provider } from "react-redux";
import store from "./store/reducer/store";
import PokemonList from "./components/PokemonList"

function App() {
  return (
    <Provider store={store}>
      <PokemonList/>
    </Provider>
  )
}

export default App
