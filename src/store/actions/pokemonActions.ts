import {loadPokemons} from '../services/pokemonService';
import {FETCH_POKEMONS_SUCCESS, FETCH_POKEMONS_ERROR } from "../type";
import { ThunkDispatch } from "redux-thunk";
import { Action } from "redux";


export const loadPokemonsList = () =>{
    return (dispatch: ThunkDispatch<{}, {}, Action>) =>{
        // Call the loadPokemons function
        loadPokemons().then((res)=>{
            // Dispatch an action of type FETCH_POKEMONS_SUCCESS with the received results
            dispatch({type:FETCH_POKEMONS_SUCCESS,res});
        },
        error=>{
            // Dispatch an action of type FETCH_POKEMONS_ERROR with the received error
            dispatch({type:FETCH_POKEMONS_ERROR,error})
        }    
        )
    }
}



