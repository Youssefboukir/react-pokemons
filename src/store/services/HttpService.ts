class HttpService {
    private url = "https://pokeapi.co/api/v2";
  
    async getData(added_url: string) {
      let requestOptions = {
        method: "GET",
      };
      return fetch(this.url + "/" + added_url, requestOptions).then((response) =>
        response.json()
      );
    }
  }

  export default HttpService;
