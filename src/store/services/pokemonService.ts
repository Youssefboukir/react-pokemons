import HttpService from './HttpService';


export const loadPokemons = () =>{
    let pokemonsDataUrl = "pokemon/"; 
    const http = new HttpService();
    // Make a GET request to the specified URL using the HttpService instance
    return http.getData(pokemonsDataUrl).then((data)=>{
        // Return the resolved data
        return data
    }).catch((error)=>{
        // Return a rejected Promise with the error
        return Promise.reject(error);
    })
    
}