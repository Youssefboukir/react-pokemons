
import { Pokemon } from "../../models/Pokemon";
import { FETCH_POKEMONS_SUCCESS, FETCH_POKEMONS_ERROR } from "../type";

const initialState = {
  pokemons: [],
  loading: false,
  error: null,
};

interface PokemonActionTypes{
    type: string,
    res: {
      results?: Array<Pokemon>,
    }
}

const PokemonsReducer = (state = initialState, action: PokemonActionTypes) => {
    switch (action.type) {
      case FETCH_POKEMONS_SUCCESS:
        // Update the state with the received results and set loading to false and error to null
        return {
          ...state,
          pokemons: action.res.results,
          loading: false,
          error: null,
        };
      case FETCH_POKEMONS_ERROR:
        // Update the state to indicate a failure and set loading to false and error with a string message
        return {
          ...state,
          loading: false,
          error: "An error has occurred while attempting to retrieve Pokémon data from the API.",
        };
      default:
        return state;
    }
  };
  
  export default PokemonsReducer;