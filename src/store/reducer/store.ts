import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import PokemonsReducer from "./PokemonsReducer";

const rootReducer = combineReducers({
  pokemon: PokemonsReducer,
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;