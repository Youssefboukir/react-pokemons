import { Component } from "react";
import { connect } from "react-redux";
import { loadPokemonsList } from "../store/actions/pokemonActions";
import { Pokemon } from "../models/Pokemon"
import Modal from 'react-bootstrap/Modal';
import { Button } from "react-bootstrap";
import ListGroup from 'react-bootstrap/ListGroup';
import { ThunkDispatch } from "redux-thunk";
import { Action } from "redux";

interface Props {
  pokemons: Pokemon[];
  loading: boolean;
  error: string;
  loadPokemonsList: () => void;
}

interface State {
  pokemon: {
    pokemons: Array<Pokemon>,
    loading: boolean,
    error: string
  }
}

class PokemonList extends Component<Props> {
  componentDidMount() {
    this.props.loadPokemonsList();
  }

  componentDidUpdate() {
  }

  // State to store the selected Pokemon and toggle the visibility of the modal
  state = {
    selectedPokemon: null as Pokemon | null,
    modalOpen: false,
  };

  // Set the selected Pokemon and open the modal when a row is clicked
  handleRowClick = (pokemon: Pokemon) => {
    this.setState({
      selectedPokemon: pokemon,
      modalOpen: true
    })
  }

  // Close the modal and clear the selected Pokemon when the close button is clicked
  handleModalClose = () => {
    this.setState({
      selectedPokemon: null,
      modalOpen: false,
    });
  };

  render() {
    const { pokemons, loading, error } = this.props;
    const { selectedPokemon, modalOpen } = this.state;

    // Display "Loading..." while data is being retrieved
    if (loading) {
      return <div>Loading...</div>;
    }

    // Display error message if there was an issue retrieving data
    if (error) {
      return <div style={{color:"red"}}>{error}</div>;
    }

    return (
      <>
      <div style={{ overflow: 'auto', maxHeight: '400px',width:'300px' }}>
        {/*Displaying list of Pokemon */}
        <ListGroup>
          {pokemons.map((pokemon) => (
            <ListGroup.Item
              key={pokemon.name}
              action
              onClick={() => {
                this.handleRowClick(pokemon);
              }}
            >
              {pokemon.name}
            </ListGroup.Item>
          ))}
        </ListGroup>
        </div>
        {/*Modal for displaying selected Pokemon details*/}
        <Modal show={modalOpen}>
          <Modal.Header closeButton>
            <Modal.Title>Detail</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            name : {selectedPokemon?.name}<br/>
            Url : {selectedPokemon?.url}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleModalClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state: State) => ({
  pokemons: state.pokemon.pokemons,// Map the pokemons property from the state to the pokemons property in the returned object
  loading: state.pokemon.loading,// Map the loading property from the state to the loading property in the returned object
  error: state.pokemon.error,// Map the error property from the state to the error property in the returned object
});

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, Action>) => ({
  // Return an object that maps the loadPokemonsList function to the loadPokemonsList prop
  loadPokemonsList: () => dispatch(loadPokemonsList()),
});

export default connect(mapStateToProps, mapDispatchToProps)(PokemonList);